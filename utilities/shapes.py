import numpy as N

# Module to create vertex arrays for shapes

# returns the vertex array with each vertex:
# x,y,z,1,    # position
# x,y,z,0,    # normal
# x,y,z,0,    # tangent
# x,y,z,0,    # bitangent = normal x tangent
# u,v,        # texture
# and the elements array 

def rectangle(width, height):
    """Returns a rectangle in the x-y plane facing +z"""
    halfWidth = width*0.5
    halfHeight = height*0.5
    v00 = (-halfWidth, -halfHeight, 0, 1)
    v01 = (-halfWidth, halfHeight, 0, 1)
    v10 = (halfWidth, -halfHeight, 0, 1)
    v11 = (halfWidth, halfHeight, 0, 1)
    n = (0,0,1,0)
    t = (1,0,0,0)
    b = (0,1,0,0)

    verts = N.array(
        v00 + n + t + b + (0,0) + # 0
        v11 + n + t + b + (1,1) + # 1
        v01 + n + t + b + (0,1) + # 2
        v00 + n + t + b + (0,0) + # 3
        v10 + n + t + b + (1,0) + # 4
        v11 + n + t + b + (1,1) , # 5
        dtype=N.float32)

    indices = N.array((0,1,2,
                       3,4,5), dtype=N.float32)

    return (N.array(verts, dtype=N.float32),
            N.array(indices,dtype=N.uint16))

def triangle(width, height):
    """Returns a triangle in the x-y plane facing +z"""
    pass

def tetrahedron(size = 1.0):
    v0 = (0.35*size,  0.5*size, 0.0,      1.0)
    v1 = (0.35*size, -0.5*size, 0.0,      1.0)
    v2 = (-0.35*size, 0.0,      0.5*size, 1.0)
    v3 = (-0.35*size, 0.0,     -0.5*size, 1.0)
    
    n0 = ( 0.5,  0.0,    0.866, 0.0)
    t0 = ( 0.866,  0.0,   -0.5, 0.0)
    b0 = ( 0.000,  1.0,    0.000, 0.0)
    n1 = ( 0.5,  0.0,   -0.866, 0.0)
    t1 = (-0.707,  0.0,   -0.707, 0.0)
    b1 = ( 0.0,    1.0,    0.0,   0.0)
    n2 = (-0.866,  0.5,  0.0,   0.0)
    t2 = ( 0.0,    0.0,    1.0,   0.0)
    b2 = ( 0.5,  0.866,  0.0,   0.0)
    n3 = (-0.866, -0.5,  0.0,   0.0)
    t3 = ( 0.0,    0.0,    1.0,   0.0)
    b3 = (-0.5,  0.866,  0.0,   0.0)

    uvll = (0,0)
    uvlr = (1,0)
    uvul = (0,1)
    uvur = (1,1)

    verts = N.array(
        v0 + n0 + t0 + b0 + uvul +
        v2 + n0 + t0 + b0 + uvll +
        v1 + n0 + t0 + b0 + uvlr +
        v0 + n1 + t1 + b1 + uvul +
        v1 + n1 + t1 + b1 + uvlr +
        v3 + n1 + t1 + b1 + uvur +
        v0 + n2 + t2 + b2 + uvur +
        v3 + n2 + t2 + b2 + uvul +
        v2 + n2 + t2 + b2 + uvlr +
        v1 + n3 + t3 + b3 + uvll +
        v2 + n3 + t3 + b3 + uvlr +
        v3 + n3 + t3 + b3 + uvul, dtype=N.float32)

    indices = N.array((0,1,2,
                       3,4,5,
                       6,7,8,
                       9,10,11), dtype=N.float32)

    return (N.array(verts, dtype=N.float32),
            N.array(indices,dtype=N.uint16))

# Module to create vertex arrays for parametric surfaces.
# point is a function(s,t) -> (x,y,z,1)
# normal is a function(s,t) -> (x,y,z,0)
# tangent is a function(s,t) -> (x,y,z,0)
# binormal is computed assuming normal and tangent are orthonormal
# texture is a function(s,t) -> (u,v)
# s and t use N.linspace to generate num points min <= x <= max

def pSurface(point, normal, tangent, 
             texture, smin, smax, snum, tmin, tmax, tnum):
    snum += 1
    tnum += 1
    verts = []
    for s in N.linspace(smin, smax, snum):
        for t in N.linspace(tmin, tmax, tnum):
            p00 = point(s,t)            
            n00 = normal(s,t)
            t00 = tangent(s,t)
            b00 = list(N.cross(N.array(n00[0:3],dtype=N.float32), 
                               N.array(t00[0:3],dtype=N.float32))) + [0]
            uv00 = texture(s,t)           
            verts.extend(p00+n00+t00+b00+uv00)
    jump = tnum
    indices = []
    for row in range(snum-1):
        for col in range(tnum-1):
            index = row*jump + col
            i00 = index
            i01 = index+1
            i10 = index+jump
            i11 = index+jump+1
            indices.extend([i00,i10,i01,i10,i11,i01])
    return (N.array(verts, dtype=N.float32),
            N.array(indices,dtype=N.uint16))

def interp(a, b, pct):
    ft = pct*3.14159
    f = (1.0-N.cos(ft))*0.5
    return a*(1.0-f) + b*(f);    
    
def mynoise(x, y):
    modfactor = 512.0
    result = (x*223.234 + y*744.12 + x*y*432.12 + 254.321) % modfactor
    return result/modfactor

def interpNoise(x,y):
    ix = N.floor(x)
    iy = N.floor(y)
    v1 = mynoise(ix,iy)
    v2 = mynoise(ix+1.0,iy)
    v3 = mynoise(ix,iy+1.0)
    v4 = mynoise(ix+1.0,iy+1.0)
    i1 = interp(v1,v2,x)
    i2 = interp(v3,v4,x)
    return interp(i1,i2,y)    

def noise(x,y):
    # Pink Noise
    total = 0.0;
    pers = 0.5;
    numOctaves = 6;
    freq = 1.0;
    amp = 0.5;
    for i in range(numOctaves):
        freq *= 2.0
        amp *= pers
        total += amp*interpNoise(x*freq, y*freq)
    return total

def terrainPoint(longangle, latangle):
    return [longangle, noise(longangle, latangle), -latangle, 1.0]

def terrainNormal(longangle, latangle):
    return [0.0,1.0,0.0,0.0]

def terrainTangent(longangle, latangle):
    return [0.0,0.0,1.0,0.0]

def terrainTexture(longangle, latangle):
    return [longangle, latangle]

def terrain(width, depth, nlongs, nlats):
    twopi = 2.0*N.pi
    halfpi = 0.5*N.pi
    return pSurface(lambda s,t:terrainPoint(s,t),
                    terrainNormal,
                    terrainTangent,
                    terrainTexture,
                    0.0, width, nlongs,
                    0.0, depth, nlats)
    
# Example using a sphere
def spherePoint(radius, longangle, latangle):
    clat = N.cos(latangle)
    slat = N.sin(latangle)
    clong = N.cos(longangle)
    slong = N.sin(longangle)
    x = radius*clong*clat
    y = radius*slat
    z = -radius*slong*clat
    return [x,y,z,1.0] # return homogeneous point

def sphereNormal(longangle, latangle):
    norm = spherePoint(1.0, longangle, latangle)
    norm[3] = 0.0
    return norm # return homogeneous vector

def sphereTangent(longangle, latangle):
    clong = N.cos(longangle)
    slong = N.sin(longangle)
    return [-slong, 0, -clong, 0]

def sphereTexture(longangle, latangle):
    return [0.5*longangle/N.pi, latangle/N.pi+0.5]

def sphere(radius, nlongs, nlats):
    twopi = 2.0*N.pi
    halfpi = 0.5*N.pi
    return pSurface(lambda s,t:spherePoint(radius,s,t),
                    sphereNormal,
                    sphereTangent,
                    sphereTexture,
                    0.0, twopi, nlongs,
                    -halfpi, halfpi, nlats)

def torusPoint(majorRadius, minorRadius, s, t):
    clong = N.cos(s)
    slong = N.sin(s)
    clat = N.cos(t)
    slat = N.sin(t)
    # find point on major circle:
    mcp = majorRadius * N.array((clong, slong, 0.0), dtype=N.float32)
    # find curve normal
    n = N.array((clong, slong, 0.0), dtype=N.float32)
    # find curve binormal
    b = N.array((0.0, 0.0, 1.0), dtype=N.float32)
    # find point on torus
    p = mcp + minorRadius*clat*n + minorRadius*slat*b
    # return homogeneous point
    return list(p) + [1.0] 

def torusNormal(s, t):
    clong = N.cos(s)
    slong = N.sin(s)
    clat = N.cos(t)
    slat = N.sin(t)
    # find curve normal
    n = N.array((clong, slong, 0.0), dtype=N.float32)
    # find curve binormal
    b = N.array((0.0, 0.0, 1.0), dtype=N.float32)
    # find vector
    v = clat*n + slat*b
    v /= N.linalg.norm(v)
    return list(v) + [0.0]

def torusTangent(s, t):
    clong = N.cos(t)
    slong = N.sin(t)
    v = [-slong, clong, 0.0, 0.0]
    return v

def torusTexture(s, t):
    return [0.5*s/N.pi,0.5*t/N.pi]    

def torus(majorRadius, minorRadius, nlongs, nlats):
    twopi = 2.0*N.pi
    return pSurface(lambda s,t:torusPoint(majorRadius, minorRadius, s, t),
                    torusNormal,
                    torusTangent,
                    torusTexture,
                    0.0, twopi, nlongs,
                    0.0, twopi, nlats)
def cylPoint(radius, longangle, latangle):
    cos_t = N.cos(latangle)
    sin_t = N.sin(latangle)
    cos_s = N.cos(longangle)
    sin_s = N.sin(longangle)
    x = radius*cos_s
    y = radius*sin_s
    z = radius*latangle    
    return [x,y,z,1.0] # return homogeneous point

def cylNormal(longangle, latangle):
    cos_t = N.cos(latangle)
    sin_t = N.sin(latangle)
    cos_s = N.cos(longangle)
    sin_s = N.sin(longangle)    
    
    dfds = N.array([-sin_s, cos_s, 0.0])
    dfdt = N.array([0.0, 0.0, 1.0])
    
    cross = N.cross(dfds, dfdt) + [0.0]
    norm = [cross[0], cross[1], cross[2], 0.0]
    return norm # return homogeneous vector

def cylTangent(longangle, latangle):
    cos_t = N.cos(latangle)
    sin_t = N.sin(latangle)
    cos_s = N.cos(longangle)
    sin_s = N.sin(longangle) 
    return [-sin_s, cos_s, 0.0, 0.0]

def cylTexture(longangle, latangle):
    return [0.5*longangle/N.pi, latangle/N.pi+0.5]

def cylinder(radius, height, nlongs, nlats):
    twopi = 2.0*N.pi
    return pSurface(lambda s,t:cylPoint(radius,s,t),
                    cylNormal,
                    cylTangent,
                    cylTexture,
                    0.0, twopi, nlongs,
                    0.0, height, nlats)

def circlePoint(radius, longangle, latangle):
    cos_t = N.cos(latangle)
    sin_t = N.sin(latangle)
    cos_s = N.cos(longangle)
    sin_s = N.sin(longangle)
    x = radius*cos_t
    y = radius*sin_t
    z = 0.0    
    return [x,y,z,1.0] # return homogeneous point

def circleNormal(longangle, latangle):
    return [0.0, 0.0, 1.0, 0.0] # return homogeneous vector

def circleTangent(longangle, latangle):
    sin_t = N.sin(latangle)
    cos_t = N.cos(latangle)
    return [-sin_t, cos_t, 0.0, 0.0]

def circleTexture(longangle, latangle):
    return [0.5*longangle/N.pi, latangle/N.pi+0.5]

def circle(radius, nlongs, nlats):
    twopi = 2.0*N.pi
    return pSurface(lambda s,t:circlePoint(radius,s,t),
                    circleNormal,
                    circleTangent,
                    circleTexture,
                    0.0, twopi, nlongs,
                    0.0, twopi, nlats)

# Castle Objects

def wall(width, height, thickness):
    # Wall facing +z direction with crenelations
    # f = front, b = back, l = left, r = right, u = up, d = down
    halfWidth = width*0.5
    halfHeight = height*0.5
    halfThick = thickness*0.5
    
    # VERTICES
    f00 = (-halfWidth, -halfHeight, halfThick, 1)
    f01 = (-halfWidth, halfHeight, halfThick, 1)
    f10 = (halfWidth, -halfHeight, halfThick, 1)
    f11 = (halfWidth, halfHeight, halfThick, 1)
    
    b00 = (-halfWidth, -halfHeight, -halfThick, 1)
    b01 = (-halfWidth, halfHeight, -halfThick, 1)
    b10 = (halfWidth, -halfHeight, -halfThick, 1)
    b11 = (halfWidth, halfHeight, -halfThick, 1)
    
    l00 = (-halfWidth, -halfHeight, halfThick, 1)
    l01 = (-halfWidth, halfHeight, halfThick, 1)
    l10 = (-halfWidth, -halfHeight, -halfThick, 1)
    l11 = (-halfWidth, halfHeight, -halfThick, 1)    
    
    r00 = (halfWidth, -halfHeight, halfThick, 1)
    r01 = (halfWidth, halfHeight, halfThick, 1)
    r10 = (halfWidth, -halfHeight, -halfThick, 1)
    r11 = (halfWidth, halfHeight, -halfThick, 1)    
    
    u00 = (-halfWidth, halfHeight, halfThick, 1)
    u01 = (-halfWidth, halfHeight, -halfThick, 1)
    u10 = (halfWidth, halfHeight, halfThick, 1)
    u11 = (halfWidth, halfHeight, -halfThick, 1)    
    
    d00 = (-halfWidth, -halfHeight, halfThick, 1)
    d01 = (-halfWidth, -halfHeight, -halfThick, 1)
    d10 = (halfWidth, -halfHeight, halfThick, 1)
    d11 = (halfWidth, -halfHeight, -halfThick, 1)        
    
    # NORMALS
    f_n = (0,0,1,0)
    b_n = (0,0,-1,0)
    r_n = (1,0,0,0)
    l_n = (-1,0,0,0)
    u_n = (0,1,0,0)
    d_n = (0,-1,0,0)
    
    # TANGENTS
    f_t = (1,0,0,0)
    b_t = (1,0,0,0)
    r_t = (0,0,-1,0)
    l_t = (0,0,1,0)
    u_t = (1,0,0,0)
    d_t = (1,0,0,0)
    
    # BITANGENTS
    f_b = (0,1,0,0)
    b_b = (0,1,0,0)
    r_b = (0,1,0,0)
    l_b = (0,-1,0,0)
    u_b = (0,0,-1,0)
    d_b = (0,0,1,0)    
    
    uvll = (0,0)
    uvrl = (1,0)
    uvlu = (0,1)
    uvru = (1,1)    
    
    verts = N.array(
        f00 + f_n + f_t + f_b + uvll +
        f11 + f_n + f_t + f_b + uvru +
        f01 + f_n + f_t + f_b + uvlu +
        f00 + f_n + f_t + f_b + uvll +
        f10 + f_n + f_t + f_b + uvrl +
        f11 + f_n + f_t + f_b + uvru + 
        
        b00 + b_n + b_t + b_b + uvll +
        b01 + b_n + b_t + b_b + uvlu +
        b11 + b_n + b_t + b_b + uvru +
        b00 + b_n + b_t + b_b + uvll +
        b11 + b_n + b_t + b_b + uvru +
        b10 + b_n + b_t + b_b + uvrl + 
        
        l00 + l_n + l_t + l_b + uvll +
        l01 + l_n + l_t + l_b + uvlu +
        l11 + l_n + l_t + l_b + uvru +
        l00 + l_n + l_t + l_b + uvll +
        l11 + l_n + l_t + l_b + uvru + 
        l10 + l_n + l_t + l_b + uvrl +
        
        r00 + r_n + r_t + r_b + uvll +
        r11 + r_n + r_t + r_b + uvru +
        r01 + r_n + r_t + r_b + uvlu +
        r00 + r_n + r_t + r_b + uvll +
        r10 + r_n + r_t + r_b + uvrl +
        r11 + r_n + r_t + r_b + uvru + 
        
        u00 + u_n + u_t + u_b + uvll +
        u11 + u_n + u_t + u_b + uvru +
        u01 + u_n + u_t + u_b + uvlu +
        u00 + u_n + u_t + u_b + uvll +
        u10 + u_n + u_t + u_b + uvrl +
        u11 + u_n + u_t + u_b + uvru + 
        
        d00 + d_n + d_t + d_b + uvll +
        d01 + d_n + d_t + d_b + uvlu +
        d11 + d_n + d_t + d_b + uvru +        
        d00 + d_n + d_t + d_b + uvll +
        d11 + d_n + d_t + d_b + uvru +
        d10 + d_n + d_t + d_b + uvrl
        
    
        , dtype=N.float32)
    
    elements = N.array((0,1,2,
                       3,4,5,
                       6,7,8,
                       9,10,11,
                       12,13,14,
                       15,16,17,
                       18,19,20,
                       21,22,23,
                       24,25,26,
                       27,28,29,
                       30,31,32,
                       33,34,35), dtype=N.float32)
    
    return (N.array(verts, dtype=N.float32),
            N.array(elements,dtype=N.uint16))

def drumTower(radius, nlongs, nlats):
    # Round Tower with crenelations
    pass

def rectTower(width, height, thickness):
    # Tower facing +z direction with crenelations around edges
    # f = front, b = back, l = left, r = right, u = up, d = down
    halfWidth = width*0.5
    halfHeight = height*0.5
    halfThick = thickness*0.5
    
    # VERTICES
    f00 = (-halfWidth, -halfHeight, halfThick, 1)
    f01 = (-halfWidth, halfHeight, halfThick, 1)
    f10 = (halfWidth, -halfHeight, halfThick, 1)
    f11 = (halfWidth, halfHeight, halfThick, 1)
    
    b00 = (-halfWidth, -halfHeight, -halfThick, 1)
    b01 = (-halfWidth, halfHeight, -halfThick, 1)
    b10 = (halfWidth, -halfHeight, -halfThick, 1)
    b11 = (halfWidth, halfHeight, -halfThick, 1)
    
    l00 = (-halfWidth, -halfHeight, halfThick, 1)
    l01 = (-halfWidth, halfHeight, halfThick, 1)
    l10 = (-halfWidth, -halfHeight, -halfThick, 1)
    l11 = (-halfWidth, halfHeight, -halfThick, 1)    
    
    r00 = (halfWidth, -halfHeight, halfThick, 1)
    r01 = (halfWidth, halfHeight, halfThick, 1)
    r10 = (halfWidth, -halfHeight, -halfThick, 1)
    r11 = (halfWidth, halfHeight, -halfThick, 1)    
    
    u00 = (-halfWidth, halfHeight, halfThick, 1)
    u01 = (-halfWidth, halfHeight, -halfThick, 1)
    u10 = (halfWidth, halfHeight, halfThick, 1)
    u11 = (halfWidth, halfHeight, -halfThick, 1)    
    
    d00 = (-halfWidth, -halfHeight, halfThick, 1)
    d01 = (-halfWidth, -halfHeight, -halfThick, 1)
    d10 = (halfWidth, -halfHeight, halfThick, 1)
    d11 = (halfWidth, -halfHeight, -halfThick, 1)        
    
    # NORMALS
    f_n = (0,0,1,0)
    b_n = (0,0,-1,0)
    r_n = (1,0,0,0)
    l_n = (-1,0,0,0)
    u_n = (0,1,0,0)
    d_n = (0,-1,0,0)
    
    # TANGENTS
    f_t = (1,0,0,0)
    b_t = (1,0,0,0)
    r_t = (0,0,-1,0)
    l_t = (0,0,1,0)
    u_t = (1,0,0,0)
    d_t = (1,0,0,0)
    
    # BITANGENTS
    f_b = (0,1,0,0)
    b_b = (0,1,0,0)
    r_b = (0,1,0,0)
    l_b = (0,-1,0,0)
    u_b = (0,0,-1,0)
    d_b = (0,0,1,0)    
    
    uvll = (0,0)
    uvrl = (1,0)
    uvlu = (0,1)
    uvru = (1,1)    
    
    verts = N.array(
        f00 + f_n + f_t + f_b + uvll +
        f11 + f_n + f_t + f_b + uvru +
        f01 + f_n + f_t + f_b + uvlu +
        f00 + f_n + f_t + f_b + uvll +
        f10 + f_n + f_t + f_b + uvrl +
        f11 + f_n + f_t + f_b + uvru + 
        
        b00 + b_n + b_t + b_b + uvll +
        b01 + b_n + b_t + b_b + uvlu +
        b11 + b_n + b_t + b_b + uvru +
        b00 + b_n + b_t + b_b + uvll +
        b11 + b_n + b_t + b_b + uvru +
        b10 + b_n + b_t + b_b + uvrl + 
        
        l00 + l_n + l_t + l_b + uvll +
        l01 + l_n + l_t + l_b + uvlu +
        l11 + l_n + l_t + l_b + uvru +
        l00 + l_n + l_t + l_b + uvll +
        l11 + l_n + l_t + l_b + uvru + 
        l10 + l_n + l_t + l_b + uvrl +
        
        r00 + r_n + r_t + r_b + uvll +
        r11 + r_n + r_t + r_b + uvru +
        r01 + r_n + r_t + r_b + uvlu +
        r00 + r_n + r_t + r_b + uvll +
        r10 + r_n + r_t + r_b + uvrl +
        r11 + r_n + r_t + r_b + uvru + 
        
        u00 + u_n + u_t + u_b + uvll +
        u11 + u_n + u_t + u_b + uvru +
        u01 + u_n + u_t + u_b + uvlu +
        u00 + u_n + u_t + u_b + uvll +
        u10 + u_n + u_t + u_b + uvrl +
        u11 + u_n + u_t + u_b + uvru + 
        
        d00 + d_n + d_t + d_b + uvll +
        d01 + d_n + d_t + d_b + uvlu +
        d11 + d_n + d_t + d_b + uvru +        
        d00 + d_n + d_t + d_b + uvll +
        d11 + d_n + d_t + d_b + uvru +
        d10 + d_n + d_t + d_b + uvrl
        
    
        , dtype=N.float32)
    
    elements = N.array((0,1,2,
                       3,4,5,
                       6,7,8,
                       9,10,11,
                       12,13,14,
                       15,16,17,
                       18,19,20,
                       21,22,23,
                       24,25,26,
                       27,28,29,
                       30,31,32,
                       33,34,35), dtype=N.float32)
    
    return (N.array(verts, dtype=N.float32),
            N.array(elements,dtype=N.uint16))