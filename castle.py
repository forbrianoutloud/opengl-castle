# Brian Lee
# OpenGL Castle

import os,sys

from OpenGL.GL import *
from OpenGL.GL.shaders import compileShader, compileProgram

import pygame
from pygame.locals import *
import numpy as N
import copy as copy

sys.path.insert(0, os.path.join(".","utilities"))
from shapes import *
from transforms import *
from frames import *
from camera import Camera
from meshes import *
from framebuffer import getFramebuffer
from loadTexture import loadTexture

def readShader(filename):
    with open(os.path.join(".","shaders", filename)) as fp:
        return fp.read()

def makeShader(vertfile, fragfile):
    return compileProgram(
        compileShader(readShader(vertfile), GL_VERTEX_SHADER),
        compileShader(readShader(fragfile), GL_FRAGMENT_SHADER)
        )

def initializeVAO():
    n = 1
    vaoArray = N.zeros(n, dtype=N.uint)
    vaoArray = glGenVertexArrays(n)
    glBindVertexArray( vaoArray )

def init():
    global theMeshes, theBox, theLight, theCamera, theCameras, \
            theScreen, theTextures, theFramebuffers
    initializeVAO()
    glEnable(GL_CULL_FACE)
    glEnable(GL_DEPTH_TEST)
    width, height = theScreen.get_size()
    # SKYBOX CAMERAS HERE IF HAVING REFLECTIONS
    
    # LIGHT
    theLight = N.array((0.707, 0.707, 0.0, 0.0),dtype=N.float32)

    # OBJECTS
    theMeshes = []
    castleShader = makeShader("worley_world.vert", "worley.frag")
    
    #   Terrain
    terrainShader = makeShader("flattextured.vert","flattextured.frag")
    colorTexture = loadTexture("grass_large_dark.jpg")
    #verts, eles = terrain(width*10,height*10, 64, 64)
    verts, eles = rectangle(width,height)
    
    rect = flatTexturedMesh(colorTexture,
                       getArrayBuffer(verts),
                       getElementBuffer(eles),
                       len(eles),
                       terrainShader,
                       N.array((1.0,1.0),dtype=N.float32))
    for i in range(2):
        rect.pitch(1) # tilt to bottom facing up
    theMeshes.append(rect)
    
    crackColor = N.array((0.0,0.0,0.0,1.0), dtype=N.float32)
    
    #   Walls
    wallShader = castleShader
    wallWidth = 250
    wallHeight = 75
    wallThickness = 8
    verts, eles = wall(wallWidth, wallHeight, wallThickness)
    wallFront = proceduralMesh(crackColor,
                                100.0,
                                getArrayBuffer(verts),
                                getElementBuffer(eles),
                                len(eles),
                                wallShader)
    wallBack = copy.deepcopy(wallFront)
    wallRight = copy.deepcopy(wallFront)
    wallLeft = copy.deepcopy(wallFront)
    
    for i in range(4):
        wallBack.yaw(1)
    for i in range(2):
        wallRight.yaw(1)
        wallLeft.yaw(-1)
    wallLeft.moveBack((wallWidth*0.5)+(wallThickness*0.5))
    wallLeft.moveRight((-wallWidth*0.5)+(wallThickness*0.5))
    wallRight.moveBack((wallWidth*0.5)+(wallThickness*0.5))
    wallRight.moveRight((wallWidth*0.5)-(wallThickness*0.5))
    wallBack.moveBack(wallWidth-(wallThickness))
    
    theMeshes.append(wallFront)
    theMeshes.append(wallBack)
    theMeshes.append(wallRight)
    theMeshes.append(wallLeft)
    
    #   Towers
    towerShader = castleShader    
                         
    gateTowerWidth = 50
    gateTowerHeight = wallHeight+15
    gateTowerThickness = 20
    verts, eles = rectTower(gateTowerWidth, gateTowerHeight, gateTowerThickness)
    gateTower = proceduralMesh(crackColor,
                                100.0,
                                getArrayBuffer(verts),
                                getElementBuffer(eles),
                                len(eles),
                                towerShader)   
    theMeshes.append(gateTower)
    
    towerWidth = wallWidth*0.1
    towerHeight = wallHeight
    verts, eles = cylinder(towerWidth, 0.05*towerHeight, 64,32)
    leftfrontTower = proceduralMesh(crackColor,
                                100.0,
                                getArrayBuffer(verts),
                                getElementBuffer(eles),
                                len(eles),
                                towerShader)    
    for i in range(2):
        leftfrontTower.pitch(1)
    rightfrontTower = copy.deepcopy(leftfrontTower)
    leftbackTower = copy.deepcopy(leftfrontTower)
    rightbackTower = copy.deepcopy(leftfrontTower)
    
    leftfrontTower.moveRight(-wallWidth*0.5)
    rightfrontTower.moveRight(wallWidth*0.5)
    leftbackTower.moveRight(-wallWidth*0.5)
    rightbackTower.moveRight(wallWidth*0.5)
    leftbackTower.moveUp(wallWidth-(wallThickness*0.5))
    rightbackTower.moveUp(wallWidth-(wallThickness*0.5))    
    
    theMeshes.append(leftfrontTower)
    theMeshes.append(rightfrontTower)
    theMeshes.append(rightbackTower)
    theMeshes.append(leftbackTower)
    
    # TEXTURE FOR SKYBOX
    theTextures = []
    images = [
        "terragenposx.png",
        "terragennegx.png",
        "terragenposy.png",
        "terragennegy.png",
        "terragenposz.png",
        "terragennegz.png"
    ]
    theTextures.append( [loadTexture(img,
                                         magFilter=GL_LINEAR,
                                         wrapMode=GL_CLAMP_TO_EDGE)
                             for img in images] )    
    
    # SKYBOX   
    boxsize = 1.0
    skyboxShader = makeShader("flattextured.vert","flattextured.frag")
    verts,elements = rectangle(boxsize,boxsize)
    vertBuff = getArrayBuffer(verts)
    elemBuff = getElementBuffer(elements)
    numElem = len(elements)
    posx = flatTexturedMesh(theTextures[0][0],
                            vertBuff,
                            elemBuff,
                            numElem,
                            skyboxShader,
                            N.array((1.0,1.0),dtype=N.float32))
    negx = flatTexturedMesh(theTextures[0][1],
                            vertBuff,
                            elemBuff,
                            numElem,
                            skyboxShader,
                            N.array((1.0,1.0),dtype=N.float32))
    posy = flatTexturedMesh(theTextures[0][2],
                            vertBuff,
                            elemBuff,
                            numElem,
                            skyboxShader,
                            N.array((1.0,1.0),dtype=N.float32))
    negy = flatTexturedMesh(theTextures[0][3],
                            vertBuff,
                            elemBuff,
                            numElem,
                            skyboxShader,
                            N.array((1.0,1.0),dtype=N.float32))
    posz =  flatTexturedMesh(theTextures[0][4],
                            vertBuff,
                            elemBuff,
                            numElem,
                             skyboxShader,
                             N.array((1.0,1.0),dtype=N.float32))
    negz =  flatTexturedMesh(theTextures[0][5],
                            vertBuff,
                            elemBuff,
                            numElem,
                             skyboxShader,
                             N.array((1.0,1.0),dtype=N.float32))

    backDistance = -boxsize*0.5

    for i in range(2):
        posx.yaw(-1)
    posx.moveBack(backDistance)

    for i in range(2):
        negx.yaw(1)
    negx.moveBack(backDistance)

    for i in range(2):
        posy.pitch(-1)
    posy.moveBack(backDistance)

    for i in range(2):
        negy.pitch(1)
    negy.moveBack(backDistance)

    for i in range(4):
        posz.yaw(1)
    posz.moveBack(backDistance)

    # negz does not need to be rotated
    negz.moveBack(backDistance)

    theBox = [posx, negx, posy, negy, posz, negz]
    
    # CAMERA
    aspectRatio = float(width)/float(height)
    near = 0.01
    far = 1000.0
    lens = 2.0  # A wide lens will minimize pixels in the background
    theCamera = Camera(lens, near, far, aspectRatio)
    theCamera.moveBack(50)
    theCamera.moveUp(40)

def display(time):
    global theMeshes, theBox, theLight, theCamera
    glClearColor(0.1, 0.2, 0.3, 0.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glClear(GL_DEPTH_BUFFER_BIT)
    glDisable(GL_DEPTH_TEST)
    for mesh in theBox:
            mesh.display(theCamera.onlyRotation(),
                         theCamera.projection(),
                         None)
    glEnable(GL_DEPTH_TEST)
    for mesh in theMeshes: 
        mesh.display(theCamera.view(),
                             theCamera.projection(),
                             theLight)        
def main():
    global theCamera, theScreen, theMeshes,\
           theTextures, theBox
    pygame.init()
    pygame.mouse.set_cursor(*pygame.cursors.broken_x)
    width, height = 1024,768
    theScreen = pygame.display.set_mode((width, height), 
                                        OPENGL|DOUBLEBUF)
    init()    
    clock = pygame.time.Clock()
    time = 0.0
    while True:
            clock.tick(30)
            time += 0.01
            # Event queued input
            for event in pygame.event.get():
                if event.type == QUIT:
                    return
                if event.type == KEYUP and event.key == K_ESCAPE:
                    return
                
            # Polling input is better for a real time camera
            pressed = pygame.key.get_pressed()
            # keys for zoom:
            if pressed[K_z]:
                theCamera.zoomIn(1.015)
            if pressed[K_x]:
                theCamera.zoomOut(1.015)
            
            # arrow keys for movement:
            movespeed = 0.3
            if pressed[K_LSHIFT]:
                movespeed *= 4
            if pressed[K_d] | pressed[K_RIGHT]:
                theCamera.moveRight(movespeed)
            if pressed[K_a] | pressed[K_LEFT]:
                theCamera.moveRight(-movespeed)
            if pressed[K_w] | pressed[K_UP]:
                theCamera.moveBack(-movespeed)
            if pressed[K_s] | pressed[K_DOWN]:
                theCamera.moveBack(movespeed)
            if pressed[K_q]:
                theCamera.pan(0.1)
            if pressed[K_e]:
                theCamera.pan(-0.1)
            if pressed[K_SPACE]:
                theCamera.moveUp(movespeed)
            if pressed[K_v]:
                theCamera.moveDown(movespeed)                
                
            # mouse for rotation
            rotspeed = 0.1
            mousespeed = 0.5*rotspeed
            x,y = pygame.mouse.get_pos()
            if (x > 0) & (y > 0):
                xDisplacement = x - 0.5*width
                yDisplacement = y - 0.5*height
                # normalize:
                xNormed = xDisplacement/width
                yNormed = -yDisplacement/height
                newx = int(x - xDisplacement*mousespeed)
                newy = int(y - yDisplacement*mousespeed)
                if (newx != x) | (newy != y):
                    theCamera.pan(-xNormed * rotspeed)
                    theCamera.tilt(-yNormed * rotspeed)
                    pygame.mouse.set_pos((newx,newy))
    
            display(time)
            pygame.display.flip()
            
if __name__ == '__main__':
    try:
        main()
    except RuntimeError, err:
        for s in err:
            print s
        raise RuntimeError(err)
    finally:
        pygame.quit()