Author: Brian Francis Lee

Description: Castle using OpenGL with Skybox. 

Requirements: Pygame, Numpy, PyOpenGL

Instructions: Run castle.py

Controls: 

	Exit 		-> Esc.
	Look Around 	-> Move Mouse
	Zoom In 	-> x
	Zoom Out 	-> z
	Speed Up 	-> Shift
	Strafe Left 	-> a or Left Arrow Key
	Strafe Right 	-> d or Right Arrow Key
	Forward 	-> w or Up Arrow Key
	Backward 	-> s or Down Arrow Key
	Turn Left 	-> q
	Turn Right 	-> e 
	Ascend 		-> Space
	Descend 	-> v
	